package repository

import (
	models "test.com/domain/models"
)

//ICategory ...
type ICategory interface {
	AddCategory(category models.Category) (*models.Category, error)

	UpdateCategory(categoryID int64, category models.Category) (*models.Category, error)

	DeleteCategory(categoryID int64) (*models.Category, error)

	GetCategories() ([]*models.Category, error)

	GetProductsOfCategory(categoryID int64) ([]*models.Product, error)
}
