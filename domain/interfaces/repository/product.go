package repository

import (
	models "test.com/domain/models"
)

//IProduct ...
type IProduct interface {
	GetProductByID(productID int64) (*models.Product, error)

	AddProduct(product *models.Product) (*models.Product, error)

	UpdateProduct(productID int64, product *models.Product) (*models.Product, error)

	DeleteProduct(productID int64) (*models.Product, error)
}
