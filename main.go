package main

import (
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"

	_ "github.com/swaggo/files"

	echoSwagger "github.com/swaggo/echo-swagger"
	ahandler "test.com/app/api/handlers/auth"
	chandler "test.com/app/api/handlers/category"
	phandler "test.com/app/api/handlers/product"
	_ "test.com/docs"
	crepomock "test.com/repository/mock/category"
	prepomock "test.com/repository/mock/product"
)

func main() {
	e := echo.New()

	na := e.Group("/api/v1/")
	na.GET("categories", chandler.GetCategories)
	na.GET("categories/:id/products", chandler.GetProductsOfCategory)
	na.GET("products/:id", phandler.GetProduct)

	//auth
	na.GET("auth/gettoken", ahandler.GetToken)

	admin := e.Group("/api/v1/admin/")

	//categories
	admin.DELETE("categories/:id", chandler.DeleteCategory)
	admin.PUT("categories/:id", chandler.UpdateCategory)
	admin.POST("categories", chandler.NewCategory)

	//products
	admin.DELETE("products/:id", phandler.DeleteProduct)
	admin.PUT("products/:id", phandler.UpdateProduct)
	admin.POST("products", phandler.NewProduct)

	jwtConfig := middleware.JWTConfig{
		SigningKey: []byte("some key"),
	}

	admin.Use(middleware.JWTWithConfig(jwtConfig))

	//documentation
	e.GET("/swagger/*", echoSwagger.WrapHandler)

	//DI configuration
	chandler.InitRepo(new(crepomock.RepositoryMock))
	phandler.InitRepo(new(prepomock.ProductRepositoryMock))
	// Start server
	e.Logger.Fatal(e.Start(":8080"))

	//r.Post("/v2/products", phandler.StoreProductsPost)
	//r.Delete("/v2/products/{productId}", phandler.StoreProductsProductIdDelete)
	//r.Get("/v2/products/{productId}", phandler.StoreProductsProductIdGet)
	//r.Put("/v2/products/{productId}", phandler.StoreProductsProductIdPut)
}
