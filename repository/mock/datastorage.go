package mock

//this is fake data sourse, can be used for development and testing, data structure just reflet API, in general should be structured better
import (
	models "test.com/domain/models"
)

var c1 = models.Category{
	ID:   1,
	Name: "Category 1",
}

var c2 = models.Category{
	ID:   2,
	Name: "Category 2",
}

var c3 = models.Category{
	ID:   3,
	Name: "Category 3",
}

var c4 = models.Category{
	ID:   4,
	Name: "category 4",
}

//Categories ...
var Categories = []*models.Category{
	&c1,
	&c2,
	&c3,
	&c4,
}

//Products ...
var Products = []*models.Product{
	{
		ID:    1,
		Prize: 87,
		Name:  "Product 1",
		Categories: map[int64]*models.Category{
			c1.ID: &c1,
			c3.ID: &c3,
		},
	},
	{
		ID:    2,
		Prize: 127,
		Name:  "Product 2",
		Categories: map[int64]*models.Category{
			c2.ID: &c2,
		},
	},
	{
		ID:    3,
		Prize: 87,
		Name:  "Product 3",
		Categories: map[int64]*models.Category{
			c4.ID: &c4,
			c3.ID: &c3,
			c1.ID: &c1,
			c2.ID: &c2,
		},
	},
	{
		ID:    4,
		Prize: 876,
		Name:  "Product 4",
		Categories: map[int64]*models.Category{
			c3.ID: &c3,
			c1.ID: &c1,
			c2.ID: &c2,
		},
	},
	{
		ID:    5,
		Prize: 76,
		Name:  "Product 5",
		Categories: map[int64]*models.Category{
			c4.ID: &c4,
			c1.ID: &c1,
			c2.ID: &c2,
		},
	},
	{
		ID:    6,
		Prize: 76,
		Name:  "Product 6",
		Categories: map[int64]*models.Category{
			c4.ID: &c4,
			c1.ID: &c1,
			c2.ID: &c2,
		},
	},
}
