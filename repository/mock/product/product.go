package product

import (
	"errors"
	"time"

	"test.com/domain/models"
	"test.com/repository/mock"
	crepo "test.com/repository/mock/category"
)

//ProductRepositoryMock ...
type ProductRepositoryMock struct {
}

//GetProductByID ...
func (c *ProductRepositoryMock) GetProductByID(productID int64) (*models.Product, error) {

	for _, p := range mock.Products {
		if p.ID == productID {
			return p, nil
		}
	}

	return nil, nil
}

//AddProduct ...
func (c *ProductRepositoryMock) AddProduct(product *models.Product) (*models.Product, error) {
	//Emulate some activities
	id := time.Now().UnixNano()
	product.ID = id

	//This is not the best solution O(n) ~ n^2
	if product.Categories != nil {
		var cr crepo.RepositoryMock
		for _, pc := range mock.Categories {
			c := cr.GetCategoryByID(pc.ID)

			if c == nil {
				return nil, errors.New(string(pc.ID) + " is missing")
			}
		}
	}

	mock.Products = append(mock.Products, product)

	for _, c := range mock.Products {
		if c.ID == id {
			return c, nil
		}
	}

	return nil, errors.New("Oops, something went wron")
}

//UpdateProduct ...
func (c *ProductRepositoryMock) UpdateProduct(productID int64, product *models.Product) (*models.Product, error) {
	if product.Categories != nil {
		var cr crepo.RepositoryMock
		for _, pc := range mock.Categories {
			c := cr.GetCategoryByID(pc.ID)

			if c == nil {
				return nil, errors.New(string(pc.ID) + " is missing")
			}
		}
	}

	//some updation here
	for _, c := range mock.Products {
		if c.ID == productID {
			c.Name = product.Name
			c.Categories = product.Categories
			return c, nil
		}

	}
	return nil, nil
}

//DeleteProduct ...
func (c *ProductRepositoryMock) DeleteProduct(productID int64) (*models.Product, error) {
	var itemToDelete *models.Product = nil
	var index int = -1
	for i, c := range mock.Products {
		if c.ID == productID {
			itemToDelete = c
			index = i
			break
		}
	}
	if index >= 0 {
		mock.Products = removeIndex(mock.Products, index)
		return itemToDelete, nil
	}

	return nil, errors.New("Oops, something went wron")
}

//Helpers
func removeIndex(s []*models.Product, index int) []*models.Product {
	return append(s[:index], s[index+1:]...)
}
