package category

import (
	"errors"
	"time"

	models "test.com/domain/models"
	mock "test.com/repository/mock"
)

//RepositoryMock ...
type RepositoryMock struct {
}

//AddCategory ...
func (c *RepositoryMock) AddCategory(category models.Category) (*models.Category, error) {
	//Emulate some activities
	id := time.Now().UnixNano()
	category.ID = id

	mock.Categories = append(mock.Categories, &category)

	for _, c := range mock.Categories {
		if c.ID == id {
			return c, nil
		}
	}

	return nil, errors.New("Oops, something went wron")
}

//UpdateCategory ...
func (c *RepositoryMock) UpdateCategory(categoryID int64, category models.Category) (*models.Category, error) {
	for _, c := range mock.Categories {
		if c.ID == categoryID {
			//Emulate some activities
			c.Name = category.Name
			return c, nil
		}
	}
	return nil, nil
}

//DeleteCategory ...
func (c *RepositoryMock) DeleteCategory(categoryID int64) (*models.Category, error) {
	var itemToDelete *models.Category = nil
	var index int = -1
	for i, c := range mock.Categories {
		if c.ID == categoryID {
			itemToDelete = c
			index = i
			break
		}
	}
	if index >= 0 {
		mock.Categories = removeIndex(mock.Categories, index)
		for _, p := range mock.Products {
			if _, ok := p.Categories[categoryID]; ok {
				delete(p.Categories, categoryID)
			}
		}
		//TODO: remove from products
		return itemToDelete, nil
	}

	return nil, errors.New("Oops, something went wron")
}

//GetCategories ...
func (c *RepositoryMock) GetCategories() ([]*models.Category, error) {
	return mock.Categories, nil
}

//GetCategoryByID ...
func (c *RepositoryMock) GetCategoryByID(id int64) *models.Category {

	for _, c := range mock.Categories {
		if c.ID == id {
			return c
		}
	}
	return nil
}

//GetProductsOfCategory ...
func (c *RepositoryMock) GetProductsOfCategory(categoryID int64) ([]*models.Product, error) {

	var result = make([]*models.Product, 0)

	for _, p := range mock.Products {
		if _, ok := p.Categories[categoryID]; ok {
			result = append(result, p)
		}
	}

	return result, nil
}

//Helpers
func removeIndex(s []*models.Category, index int) []*models.Category {
	return append(s[:index], s[index+1:]...)
}
