module test.com

go 1.14

require (
	github.com/alecthomas/template v0.0.0-20190718012654-fb15b899a751
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/go-openapi/spec v0.19.9 // indirect
	github.com/go-openapi/swag v0.19.9 // indirect
	github.com/labstack/echo/v4 v4.1.16
	github.com/mailru/easyjson v0.7.3 // indirect
	github.com/mattn/go-colorable v0.1.7 // indirect
	github.com/swaggo/echo-swagger v1.0.0 // indirect
	github.com/swaggo/files v0.0.0-20190704085106-630677cd5c14
	github.com/swaggo/swag v1.6.7
	github.com/valyala/fasttemplate v1.2.1 // indirect
	golang.org/x/crypto v0.0.0-20200728195943-123391ffb6de // indirect
	golang.org/x/net v0.0.0-20200813134508-3edf25e44fcc // indirect
	golang.org/x/sys v0.0.0-20200812155832-6a926be9bd1d // indirect
	golang.org/x/tools v0.0.0-20200813181007-d926bd178cf0 // indirect
	gopkg.in/yaml.v2 v2.3.0 // indirect
)
