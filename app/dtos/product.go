package dtos

//ProductDTO ...
type ProductDTO struct {
	ID int64 `json:"id"`

	Prize int64 `json:"prize"`

	Name string `json:"name"`

	Categories []*CategoryDTO `json:"categories"`
}
