package dtos

//CategoryDTO ...
type CategoryDTO struct {
	ID int64 `json:"id"`

	Name string `json:"name"`
}
