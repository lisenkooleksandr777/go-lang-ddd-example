package auth

import (
	"errors"
	"net/http"
	"time"

	"github.com/dgrijalva/jwt-go"
	"github.com/labstack/echo/v4"
)

//GetToken ...
func GetToken(c echo.Context) error {

	// Set custom claims
	claims := jwt.StandardClaims{
		ExpiresAt: time.Now().Add(time.Hour * 72).Unix(),
	}

	// Create token with claims
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)

	// Generate encoded token and send it as response.
	t, err := token.SignedString([]byte("some secret"))

	if err != nil {
		return c.JSON(http.StatusInternalServerError, errors.New("=( Oops, Somehing went wrong"))
	}

	//just to smplyfy copying of token for demonstration purpose
	return c.JSON(http.StatusOK, "Bearer "+t)
}
