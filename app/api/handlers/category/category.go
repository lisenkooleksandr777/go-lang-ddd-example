package category

import (
	"errors"
	"net/http"
	"strconv"

	"github.com/labstack/echo/v4"
	categoryAdapter "test.com/app/adapter/category"
	productAdapter "test.com/app/adapter/product"
	"test.com/app/dtos"
	"test.com/domain/interfaces/repository"
)

//Configuration of DI
var cr repository.ICategory

//InitRepo Configuration of DI
func InitRepo(categoryRepository repository.ICategory) {
	cr = categoryRepository
}

// GetCategories godoc
func GetCategories(c echo.Context) error {

	var categories, err = cr.GetCategories()

	if err != nil {
		c.JSON(http.StatusInternalServerError, err)
	}
	return c.JSON(http.StatusOK, categoryAdapter.ToCategoryDTOs(categories))
}

//GetProductsOfCategory ...
func GetProductsOfCategory(c echo.Context) error {
	id := c.Param("id")
	if id == "" {
		return c.JSON(http.StatusBadRequest, errors.New("Incorrect id"))
	}

	cid, err := strconv.Atoi(id)
	if err != nil {
		return c.JSON(http.StatusBadRequest, errors.New("id should be int id"))
	}

	products, err := cr.GetProductsOfCategory(int64(cid))

	if products == nil {
		return c.JSON(http.StatusNoContent, "no data")
	}

	if err != nil {
		c.JSON(http.StatusInternalServerError, err)
	}
	return c.JSON(http.StatusOK, productAdapter.ToProductDTOs(products))
}

// DeleteCategory ...
func DeleteCategory(c echo.Context) error {
	id := c.Param("id")
	if id == "" {
		return c.JSON(http.StatusBadRequest, errors.New("Incorrect id"))
	}

	cid, err := strconv.Atoi(id)
	if err != nil {
		return c.JSON(http.StatusBadRequest, errors.New("id should be int id"))
	}

	category, err := cr.DeleteCategory(int64(cid))

	if category == nil {
		return c.JSON(http.StatusNotFound, "Not found")
	}

	if err != nil {
		c.JSON(http.StatusInternalServerError, err)
	}
	return c.JSON(http.StatusOK, categoryAdapter.ToCategoryDTO(category))
}

// UpdateCategory ...
func UpdateCategory(c echo.Context) error {
	id := c.Param("id")
	if id == "" {
		return c.JSON(http.StatusBadRequest, errors.New("Incorrect id"))
	}

	cid, err := strconv.Atoi(id)
	if err != nil {
		return c.JSON(http.StatusBadRequest, errors.New("Id should be int id"))
	}

	var updateModel dtos.CategoryDTO
	err = c.Bind(&updateModel)
	if err != nil {
		return c.JSON(http.StatusBadRequest, "Bad request")
	}

	//TODO: some validation here

	category, err := cr.UpdateCategory(int64(cid), categoryAdapter.ToCategory(&updateModel))

	if category == nil {
		return c.JSON(http.StatusNotFound, "Not found")
	}

	if err != nil {
		c.JSON(http.StatusInternalServerError, err)
	}
	return c.JSON(http.StatusOK, categoryAdapter.ToCategoryDTO(category))
}

// NewCategory ...
func NewCategory(c echo.Context) error {

	//TODO: some validation here

	var createModel dtos.CategoryDTO
	err := c.Bind(&createModel)
	if err != nil {
		return c.JSON(http.StatusBadRequest, "Bad request")
	}

	category, err := cr.AddCategory(categoryAdapter.ToCategory(&createModel))

	if category == nil {
		return c.JSON(http.StatusNotFound, "Not found")
	}

	if err != nil {
		c.JSON(http.StatusInternalServerError, err)
	}
	return c.JSON(http.StatusOK, categoryAdapter.ToCategoryDTO(category))
}
