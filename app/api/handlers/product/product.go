package product

import (
	"errors"
	"net/http"
	"strconv"

	"github.com/labstack/echo/v4"
	productAdapter "test.com/app/adapter/product"
	"test.com/app/dtos"
	"test.com/domain/interfaces/repository"
	prepository "test.com/repository/mock/product"
)

//Configuration of DI
var pr repository.IProduct

//InitRepo Configuration of DI
func InitRepo(productRepository repository.IProduct) {
	pr = productRepository
}

//GetProduct ...
func GetProduct(c echo.Context) error {
	id := c.Param("id")
	if id == "" {
		return c.JSON(http.StatusBadRequest, errors.New("Incorrect id"))
	}

	cid, err := strconv.Atoi(id)
	if err != nil {
		return c.JSON(http.StatusBadRequest, errors.New("id should be int id"))
	}

	var pr prepository.ProductRepositoryMock
	product, err := pr.GetProductByID(int64(cid))

	if product == nil {
		return c.JSON(http.StatusNotFound, "Not found")
	}

	if err != nil {
		c.JSON(http.StatusInternalServerError, err)
	}
	return c.JSON(http.StatusOK, productAdapter.ToProductDTO(product))
}

// DeleteProduct ...
func DeleteProduct(c echo.Context) error {
	id := c.Param("id")
	if id == "" {
		return c.JSON(http.StatusBadRequest, errors.New("Incorrect id"))
	}

	cid, err := strconv.Atoi(id)
	if err != nil {
		return c.JSON(http.StatusBadRequest, errors.New("id should be int id"))
	}

	category, err := pr.DeleteProduct(int64(cid))

	if category == nil {
		return c.JSON(http.StatusNotFound, "Not found")
	}

	if err != nil {
		c.JSON(http.StatusInternalServerError, err)
	}
	return c.JSON(http.StatusOK, productAdapter.ToProductDTO(category))
}

// UpdateProduct ...
func UpdateProduct(c echo.Context) error {
	id := c.Param("id")
	if id == "" {
		return c.JSON(http.StatusBadRequest, errors.New("Incorrect id"))
	}

	cid, err := strconv.Atoi(id)
	if err != nil {
		return c.JSON(http.StatusBadRequest, errors.New("Id should be int id"))
	}

	var updateModel dtos.ProductDTO
	err = c.Bind(&updateModel)
	if err != nil {
		return c.JSON(http.StatusBadRequest, "Bad request")
	}

	//TODO: some validation here
	productToUpdate := productAdapter.ToProduct(&updateModel)
	product, err := pr.UpdateProduct(int64(cid), productToUpdate)

	if product == nil {
		return c.JSON(http.StatusNotFound, "Not found")
	}

	if err != nil {
		c.JSON(http.StatusInternalServerError, err)
	}
	return c.JSON(http.StatusOK, productAdapter.ToProductDTO(product))
}

// NewProduct ...
func NewProduct(c echo.Context) error {
	//TODO: some validation here

	var createModel dtos.ProductDTO
	err := c.Bind(&createModel)
	if err != nil {
		return c.JSON(http.StatusBadRequest, "Bad request")
	}
	newProduct := productAdapter.ToProduct(&createModel)
	createdProduct, err := pr.AddProduct(newProduct)

	if createdProduct == nil {
		return c.JSON(http.StatusNotFound, "Not found")
	}

	if err != nil {
		c.JSON(http.StatusInternalServerError, err)
	}
	return c.JSON(http.StatusOK, productAdapter.ToProductDTO(createdProduct))
}
