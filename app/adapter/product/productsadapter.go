package product

import (
	categoryAdapter "test.com/app/adapter/category"
	"test.com/app/dtos"
	"test.com/domain/models"
)

//ToProduct ...
func ToProduct(dto *dtos.ProductDTO) *models.Product {
	if dto != nil && dto.Categories != nil {
		var result = &models.Product{
			ID:         dto.ID,
			Name:       dto.Name,
			Categories: make(map[int64]*models.Category),
		}

		for _, category := range dto.Categories {
			if _, ok := result.Categories[category.ID]; !ok {
				category := categoryAdapter.ToCategory(category)
				result.Categories[category.ID] = &category
			}
		}
		return result
	}
	return nil
}

//ToProductDTO ...
func ToProductDTO(category *models.Product) *dtos.ProductDTO {
	return &dtos.ProductDTO{
		ID:         category.ID,
		Name:       category.Name,
		Prize:      category.Prize,
		Categories: categoryAdapter.MapToCategoryDTOs(category.Categories),
	}
}

//ToProductDTOs ...
func ToProductDTOs(categories []*models.Product) []*dtos.ProductDTO {
	result := make([]*dtos.ProductDTO, 0)
	for _, c := range categories {
		result = append(result, ToProductDTO(c))
	}
	return result
}
