package category

import (
	"test.com/app/dtos"
	"test.com/domain/models"
)

//ToCategory ...
func ToCategory(dto *dtos.CategoryDTO) models.Category {
	return models.Category{
		ID:   dto.ID,
		Name: dto.Name,
	}
}

//ToCategoryDTO ...
func ToCategoryDTO(category *models.Category) *dtos.CategoryDTO {
	return &dtos.CategoryDTO{
		ID:   category.ID,
		Name: category.Name,
	}
}

//ToCategoryDTOs ...
func ToCategoryDTOs(categories []*models.Category) []*dtos.CategoryDTO {
	result := make([]*dtos.CategoryDTO, 0)
	for _, c := range categories {
		result = append(result, ToCategoryDTO(c))
	}
	return result
}

//MapToCategoryDTOs ...
func MapToCategoryDTOs(categories map[int64]*models.Category) []*dtos.CategoryDTO {
	result := make([]*dtos.CategoryDTO, 0)
	for _, c := range categories {
		result = append(result, ToCategoryDTO(c))
	}
	return result
}
